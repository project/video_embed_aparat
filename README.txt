"Video Embed Aparat" 
"Video Embed Aparat" 
Drupal video_embed_field integration with Aparat.com.

This module provides Aparat handler for Video Embed Field.
Users can add easily Aparat videos to their site by pasting the video's URL into a video embed field.
In the settings users can set width and height.
This module also can get thumbnail image from Aparat website for using it as a teaser image.

persian:

این افزونه، امکان افزودن فیلم از سرویس اشتراک فیلم آپارات در سایت دروپال را فراهم می کند. در واقع این افزونه با افزودن یک کلاس به افزونه Video Embed Field شما را قادر می سازد که فیلم های آپلود شده در آپارات را تنها با کپی کردن آدرس کامل آن فیلم و جایگذاری آن در فیلد ویدئویی که قبلا در نوع محتوای دلخواه خود ایجاد کرده اید، در سایت خود به نمایش در آورید.

این افزونه توسط گروه رنگینه به آدرس rangine.ir ایجاد شده است.

راهنمای نصب:

Drupal 7:

    این افزونه را دانلود و در مسیر افزونه‌های خود(معمولاً: sites/all/modules) قرار دهید. و یا از طریق نصب ماژول خود دروپال اقدام نمایید.
    اگر افزونه Video Embed Field بر روی سایت شما نصب نیست آن را هم به افزونه‌های خود اضافه نمایید.
    در صفحه افزونه‌ها(ماژول‌ها) افزونه Video Embed Aparat و Video Embed Field‌ را فعال نمایید.
    به صفحه پیکربندی Video Embed Field رفته و در بخش آپارت آن ارتفاع و عرض نمایش ویدئو را تنطیم نمایید.
    فیلدی از نوع Video Embed را در نوع محتوای مورد نظر خود ایجاد نمایید.
    حالا هنگام ایجاد محتوا از نوع محتوایی که فیلد ویدئو دارد شما می توانید لینک آدرس کامل فیلم آپارات که مشخصا در نهایت آدرس خود کد فیلم قرار دارد را وارد نمایید مانند: https://www.aparat.com/v/Lpqw0 .
    سپس محتوا را ذخیره کنید و از تماشای فیلم در سایت خود لذت ببرید.

Drupal 8:
	با تفاوت بسیار جزئی همانند نصب در دروپال 7 هست. فقط صفحه تنظیم عرض و ارتفاع ندارد و شما می توانید با css این کار را انجام دهید.
